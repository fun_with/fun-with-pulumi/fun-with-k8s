package main

import (
	"fmt"
	"github.com/pulumi/pulumi-kubernetes/sdk/v4/go/kubernetes/helm/v3"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi/config"
)

const NginxGroup = "nginx"
const NginxHelmVersion = "helm_version"

type HelmChartInfo struct {
	name            string
	version         string
	url             string
	crds            bool `default:"false"`
	createNamespace bool `default:"false"`
	values          pulumi.Map
}

func initNginx(ctx *pulumi.Context) (*helm.Release, error) {
	helmValues := pulumi.Map{
		"controller": pulumi.Map{
			"publishService": pulumi.Map{
				"enabled": pulumi.Bool(true),
			},
		},
	}

	helmInfos := HelmChartInfo{
		name:            "ingress-nginx",
		version:         nginxConfig(ctx, NginxHelmVersion),
		url:             "https://kubernetes.github.io/ingress-nginx",
		createNamespace: true,
		crds:            true,
		values:          helmValues,
	}
	nginxHelmRelease, err := initHelm(ctx, helmInfos, "nginx-ingress-controller")
	if err != nil {
		return nil, err
	}
	return nginxHelmRelease, nil
}

func initHelm(ctx *pulumi.Context, chart HelmChartInfo, namespace string) (*helm.Release, error) {
	cfg := config.New(ctx, "")
	_, err := cfg.Try(namespace)
	if err != nil && !chart.createNamespace {
		ctx.Log.Error(namespace+" namespace for "+chart.name+" Helm chart doesn't exist", nil)
		return nil, err
	}

	// Use Helm to install
	helmRelease, err := helm.NewRelease(ctx, chart.name, &helm.ReleaseArgs{
		Chart:           pulumi.String(chart.name),
		Namespace:       pulumi.String(namespace),
		CreateNamespace: pulumi.Bool(chart.createNamespace),
		RepositoryOpts: &helm.RepositoryOptsArgs{
			Repo: pulumi.String(chart.url),
		},
		SkipCrds: pulumi.Bool(chart.crds),
		Version:  pulumi.String(chart.version),
		Values:   chart.values,
	})
	if err != nil {
		return nil, err
	}
	return helmRelease, nil
}

func getConfig(ctx *pulumi.Context, group string, key string) string {
	return config.Require(ctx, fmt.Sprintf("%s:%s", group, key))
}

func nginxConfig(ctx *pulumi.Context, key string) string {
	return getConfig(ctx, NginxGroup, key)
}
