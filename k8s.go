package main

import (
	v1 "github.com/pulumi/pulumi-kubernetes/sdk/v4/go/kubernetes/apps/v1"
	k8score "github.com/pulumi/pulumi-kubernetes/sdk/v4/go/kubernetes/core/v1"
	k8smeta "github.com/pulumi/pulumi-kubernetes/sdk/v4/go/kubernetes/meta/v1"
	k8sauth "github.com/pulumi/pulumi-kubernetes/sdk/v4/go/kubernetes/rbac/v1"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

func initK8s(ctx *pulumi.Context) error {

	saArgs := k8score.ServiceAccountArgs{
		Metadata: &k8smeta.ObjectMetaArgs{
			Namespace: pulumi.String("external-dns"),
			Name:      pulumi.String("external-dns-sa"),
		},
	}
	serviceAccount, err := k8score.NewServiceAccount(ctx, "external-dns-sa", &saArgs, pulumi.DeleteBeforeReplace(true))
	if err != nil {
		return err
	}

	roles := &k8sauth.ClusterRoleArgs{
		Rules: &k8sauth.PolicyRuleArray{
			k8sauth.PolicyRuleArgs{
				ApiGroups: pulumi.StringArray{
					pulumi.String(""),
				},
				Resources: pulumi.StringArray{
					pulumi.String("services"),
					pulumi.String("endpoints"),
					pulumi.String("pods"),
				},
				Verbs: pulumi.StringArray{
					pulumi.String("get"),
					pulumi.String("watch"),
					pulumi.String("list"),
				},
			},
			k8sauth.PolicyRuleArgs{
				ApiGroups: pulumi.StringArray{
					pulumi.String("extensions"),
					pulumi.String("networking.k8s.io"),
				},
				Resources: pulumi.StringArray{
					pulumi.String("ingresses"),
				},
				Verbs: pulumi.StringArray{
					pulumi.String("get"),
					pulumi.String("watch"),
					pulumi.String("list"),
				},
			},
			k8sauth.PolicyRuleArgs{
				ApiGroups: pulumi.StringArray{
					pulumi.String(""),
				},
				Resources: pulumi.StringArray{
					pulumi.String("nodes"),
				},
				Verbs: pulumi.StringArray{
					pulumi.String("list"),
				},
			},
		},
	}

	_, err = k8sauth.NewClusterRole(ctx, "external-dns-cluster-role", roles, pulumi.DependsOn([]pulumi.Resource{serviceAccount}))
	if err != nil {
		return err
	}

	roleBinding := &k8sauth.ClusterRoleBindingArgs{
		Metadata: k8smeta.ObjectMetaArgs{
			Name: pulumi.String("external-dns-cluster-role-binding"),
		},
		RoleRef: k8sauth.RoleRefArgs{
			ApiGroup: pulumi.String("rbac.authorization.k8s.io"),
			Kind:     pulumi.String("ClusterRole"),
			Name:     pulumi.String("external-dns-cluster-role"),
		},
		Subjects: k8sauth.SubjectArray{
			k8sauth.SubjectArgs{
				Kind:      pulumi.String("ServiceAccount"),
				Name:      pulumi.String("external-dns-cluster-role"),
				Namespace: pulumi.String("external-dns"),
			},
		},
	}

	crb, err := k8sauth.NewClusterRoleBinding(ctx, "external-dns-cluster-role-binding", roleBinding, pulumi.DependsOn([]pulumi.Resource{serviceAccount}))
	if err != nil {
		return err
	}

	deployment := v1.DeploymentArgs{
		Metadata: k8smeta.ObjectMetaArgs{
			Name:      pulumi.String("external-dns-cloudflare"),
			Namespace: pulumi.String("external-dns"),
			Labels: pulumi.StringMap{
				"app.kubernetes.io/instance": pulumi.String("external-dns-cloudflare"),
				"app.kubernetes.io/name":     pulumi.String("external-dns"),
			},
		},
		Spec: v1.DeploymentSpecArgs{
			Selector: k8smeta.LabelSelectorArgs{
				MatchLabels: pulumi.StringMap{
					"app.kubernetes.io/instance": pulumi.String("external-dns-cloudflare"),
					"app.kubernetes.io/name":     pulumi.String("external-dns"),
				},
			},
			Strategy: v1.DeploymentStrategyArgs{
				Type: pulumi.String("Recreate"),
			},
			Template: k8score.PodTemplateSpecArgs{
				Metadata: k8smeta.ObjectMetaArgs{
					Labels: pulumi.StringMap{
						"app.kubernetes.io/instance": pulumi.String("external-dns-cloudflare"),
						"app.kubernetes.io/name":     pulumi.String("external-dns"),
					},
				},
				Spec: k8score.PodSpecArgs{
					ServiceAccount: pulumi.String("external-dns-sa"),
					Containers: k8score.ContainerArray{
						k8score.ContainerArgs{
							Name:  pulumi.String("external-dns-cloudflare"),
							Image: pulumi.String("registry.k8s.io/external-dns/external-dns:v0.14.0"),
							Args: pulumi.StringArray{
								pulumi.String("--source=ingress"),
								pulumi.String("--domain-filter=grunty.uk"),
								pulumi.String("--provider=cloudflare"),
								pulumi.String("--txt-prefix=$(UID)-"),
								pulumi.String("--txt-owner-id=$(UID)"),
							},
							Env: k8score.EnvVarArray{
								k8score.EnvVarArgs{
									Name: pulumi.String("CF_API_TOKEN"),
									ValueFrom: k8score.EnvVarSourceArgs{
										SecretKeyRef: k8score.SecretKeySelectorArgs{
											Name: pulumi.String("cloudflare-api-token"),
											Key:  pulumi.String("api-key"),
										},
									},
								},
								k8score.EnvVarArgs{
									Name: pulumi.String("CF_API_EMAIL"),
									ValueFrom: k8score.EnvVarSourceArgs{
										SecretKeyRef: k8score.SecretKeySelectorArgs{
											Name: pulumi.String("cloudflare-user-mail"),
											Key:  pulumi.String("user-mail"),
										},
									},
								},
								k8score.EnvVarArgs{
									Name: pulumi.String("UID"),
									ValueFrom: k8score.EnvVarSourceArgs{
										FieldRef: k8score.ObjectFieldSelectorArgs{
											FieldPath: pulumi.String("spec.nodeName"),
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
	_, err = v1.NewDeployment(ctx, "external-dns-deployment", &deployment, pulumi.DependsOn([]pulumi.Resource{crb}), pulumi.DeleteBeforeReplace(true))
	if err != nil {
		return err
	}
	return nil
}
