package main

import (
	"bytes"
	"errors"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"os"
	"os/exec"
	"slices"
)

const Kubeconfig = "KUBECONFIG"

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {

		// Checks everything is ok
		checkRequirements(ctx)

		// Nginx ingress controller
		initNginx(ctx)

		// External-DNS
		initK8s(ctx)

		return nil
	})

}

func checkRequirements(ctx *pulumi.Context) error {
	k8sVars := []string{os.Getenv(Kubeconfig)}
	if slices.Contains(k8sVars, "") {
		_ = ctx.Log.Error("❌ A mandatory variable is missing, "+
			"check that all these variables are set: KUBECONFIG", nil)
		return nil
	}

	cmd := exec.Command("kubectl", "get", "po")
	cmdOutput := &bytes.Buffer{}
	cmd.Stdout = cmdOutput
	if errors.Is(cmd.Err, exec.ErrDot) {
		cmd.Err = nil
	}
	if err := cmd.Run(); err != nil {
		_ = ctx.Log.Error("❌ kubectl command failed, cannot communicate with cluster", nil)
	}
	ctx.Log.Info("✅ Connection to cluster validated", nil)
	return nil
}
